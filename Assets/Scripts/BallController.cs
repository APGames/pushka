﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    public float force;

    public Vector3 offset;

    private Rigidbody rb;
    private CameraController cameraController;
    private MenuController menuController;
    private float XpositionStart;
    private Vector3 lastPosition;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        cameraController = Camera.main.GetComponent<CameraController>();
        menuController = FindObjectOfType<MenuController>();
        XpositionStart = transform.position.x;
        lastPosition = transform.position;
        offset = Vector3.zero;
    }

    void FixedUpdate()
    {
        offset = transform.position - lastPosition;
        transform.position = new Vector3(transform.position.x, transform.position.y, XpositionStart);
        lastPosition = transform.position;
        float vertical = Input.GetAxis("Vertical");
        rb.AddForce(Vector3.up * force * vertical);
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Obstacle"))
        {
            Destroy(gameObject);
            cameraController.DontFly();
            menuController.OpenLose();
        }

        if (collision.gameObject.CompareTag("Finish"))
        {
            Destroy(gameObject);
            cameraController.DontFly();
            menuController.OpenWin();
        }
    }
}

