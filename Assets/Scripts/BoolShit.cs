﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BoolShit : MonoBehaviour
{
    [SerializeField] private int _countWalls;
    [SerializeField] private GameObject PieceOfWall;
    [SerializeField] private GameObject TriggerPieceOfWall;
    [SerializeField] private GameObject Finnish;
    [SerializeField] private Pushka pushka;
    private GameObject UpperWall;
    private GameObject LowerWall;
    private GameObject Trigger;
    private float point1;
    private float point2;
    private System.Random rnd;
    private Wall[] walls;
    private GameObject finnish;

    private int _countWallsLeft;

    private void Start()
    {
        rnd = new System.Random();
        _countWallsLeft = _countWalls;
        walls = new Wall[_countWalls];
    }

    void Update()
    {
        if (_countWallsLeft != 0)
        {
            Trigger = Instantiate(TriggerPieceOfWall, new Vector3(0, rnd.Next(1,10), transform.position.z), new Quaternion(0, 0, 0, 0));
            point1 = Trigger.transform.position.y + Trigger.GetComponent<BoxCollider>().size.y / 2;
            point2 = 9.5f;
            if (point2 - point1 > 0.00001f)
            {
                UpperWall = Instantiate(PieceOfWall,new Vector3(0,(point2-point1)/2+point1,transform.position.z),new Quaternion(0,0,0,0));
                UpperWall.transform.localScale = new Vector3(1, point2 - point1, 1);
            }
            point1 = Trigger.transform.position.y - Trigger.GetComponent<BoxCollider>().size.y / 2;
            if (point1 > 0)
            {
                LowerWall = Instantiate(PieceOfWall, new Vector3(0, (point1)/2, transform.position.z), new Quaternion(0, 0, 0, 0));
                LowerWall.transform.localScale = new Vector3(1, point1, 1);
            }
            walls[_countWalls - _countWallsLeft] = new Wall();
            walls[_countWalls - _countWallsLeft].Trigger = Trigger;
            walls[_countWalls - _countWallsLeft].UpperWall = UpperWall;
            walls[_countWalls - _countWallsLeft].LowerWall = LowerWall;
            transform.Translate(0, 0, 80);
            _countWallsLeft--;
        }
        else
        {
            finnish = Instantiate(Finnish, new Vector3(0,4.5f,transform.position.z), new Quaternion(0,0,0,0));
            gameObject.SetActive(false);
            var objs = pushka.gameObjects;
            pushka.gameObjects = new GameObject[objs.Length + walls.Length * 3 + 1];
            for (int i = 0; i < objs.Length; i++)
            {
                pushka.gameObjects[i] = objs[i];
            }
            for (int i = 0; i < walls.Length; i++)
            {
                pushka.gameObjects[objs.Length + i * 3] = walls[i].LowerWall;
                pushka.gameObjects[objs.Length + i * 3 + 1] = walls[i].Trigger;
                pushka.gameObjects[objs.Length + i * 3 + 2] = walls[i].UpperWall;
            }
            pushka.gameObjects[objs.Length + walls.Length * 3] = finnish;
        }
    }

    public void Restart()
    {
        transform.position = new Vector3(0, 0, 80);
        _countWallsLeft = _countWalls;
        for (int i = 0; i < walls.Length; i++)
        {
            Destroy(walls[i].Trigger);
            Destroy(walls[i].UpperWall);
            Destroy(walls[i].LowerWall);
        }
        walls = new Wall[_countWalls];
        Destroy(finnish);
        gameObject.SetActive(true);
    }
}
