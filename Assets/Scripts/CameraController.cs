﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Vector3 offset;
    [Range(0f, 1f)]
    public float speed;

    private bool isFly;
    private GameObject targetObj;
    private Vector3 target;

    private void Start()
    {
        target = transform.position;
    }

    void FixedUpdate()
    {
        if (isFly)
        {
            target = targetObj.transform.position + offset;
        }

        transform.position = Vector3.Lerp(transform.position, target, speed);
    }

    public void Fly(GameObject obj)
    {
        isFly = true;
        targetObj = obj;
    }

    public void DontFly()
    {
        isFly = false;
    }
}