﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pushka : MonoBehaviour
{
    public float speed;
    public GameObject ballPrefab;
    public GameObject ballSpawner;
    public float force;
    public CameraController cameraController;
    public GameObject[] gameObjects;

    private float angle;
    private float direction;
    private bool isStay;
    private bool isShot;
    BallController ball;

    // Start is called before the first frame update
    void Start()
    {
        Restart();
    }

    private void FixedUpdate()
    {
        if (ball != null)
        {
            foreach (var item in gameObjects)
            {
                item.transform.position -= ball.offset;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.Euler(angle, 0, 90);

        if (!isStay)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                isStay = true;
            }
            angle += direction * speed * Time.deltaTime;
        }
        else if (!isShot && Input.GetKeyDown(KeyCode.Space))
        {
            ball = Instantiate(
               ballPrefab,
               ballSpawner.transform.position,
               ballSpawner.transform.rotation
           ).GetComponent<BallController>();
            var rb = ball.GetComponent<Rigidbody>();
            rb.AddForce(ball.transform.up * force, ForceMode.Impulse);
            cameraController.Fly(ball.gameObject);
            isShot = true;
        }

        if (angle > 20)
        {
            direction = -1;
            angle = 20;
        }
        else if (angle < -20)
        {
            direction = 1;
            angle = -20;
        }
    }

    public void Restart()
    {
        angle = 0;
        direction = 1;
        isStay = false;
        isShot = false;
        if (ball != null)
        {
            Destroy(ball.gameObject);
        }
    }
}
