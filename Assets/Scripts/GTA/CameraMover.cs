﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour
{
    [SerializeField] private Transform parent;
    [SerializeField] private float sencivity;

    private float angleY;
    private float angleX;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        var mouseX = Input.GetAxis("Mouse X") * sencivity;
        var mouseY = Input.GetAxis("Mouse Y") * sencivity;

        angleY += mouseX;
        angleX -= mouseY;

        angleX = Mathf.Clamp(angleX, -60, 60);

        parent.localRotation = Quaternion.Euler(0, angleY, 0);
        transform.localRotation = Quaternion.Euler(angleX, 0, 0);
    }
}
